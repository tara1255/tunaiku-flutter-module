import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tunaiku/src/ui/MainActivity.dart';

import 'src/bloc/MyBloc.dart';

void main() {
  runApp(MaterialApp(
    title: "Tunaiku",
    home: BlocProvider(
      create: (BuildContext context) => MyBloc(),
      child: MainActivity(),
    ),
    debugShowCheckedModeBanner: false,
  ));
}
