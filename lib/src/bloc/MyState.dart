import 'package:equatable/equatable.dart';

abstract class MyState extends Equatable {
  const MyState();

  @override
  List<Object> get props => [];
}

class SubmitInitial extends MyState {}

class SubmitSuccess extends MyState {}

class SubmitOnProgress extends MyState {}

class SubmitFailure extends MyState {}
