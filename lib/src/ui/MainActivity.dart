import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tunaiku/src/bloc/MyBloc.dart';
import 'package:tunaiku/src/bloc/MyEvent.dart';
import 'package:tunaiku/src/bloc/MyState.dart';
import 'package:tunaiku/src/common/MyColor.dart';

class MainActivity extends StatefulWidget {
  @override
  _MainActivityState createState() => _MainActivityState();
}

class _MainActivityState extends State<MainActivity> {
  final Map _listStarRating = {
    1.0: 'Sangat Sulit',
    2.0: 'Sulit',
    3.0: 'Biasa',
    4.0: 'Mudah',
    5.0: 'Sangat Mudah'
  };

  final List<String> _listEmotionRating = [
    'Frustasi',
    'Tertarik',
    'Puas',
    'Bingung',
    'Tenang',
    'Kecewa'
  ];

  String _selectedStarRating = '';
  String _selectedEmotionRating = '';
  var _suggestion = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // RatingBloc ratingBloc = BlocProvider.of<RatingBloc>(context);

    _onSubmitButtonPressed() {
      BlocProvider.of<MyBloc>(context).add(SubmitButtonPressed(
          starRating: '_selectedStarRating',
          emotionRating: '_selectedEmotionRating',
          suggestion: _suggestion.text));
    }

    var appBar = AppBar(
      title: Text(
        'Survei dan Saran',
        style: TextStyle(fontSize: 14, color: MyColor.white),
      ),
      leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
            size: 14,
          ),
          onPressed: () {}),
      backgroundColor: MyColor.blueAppBar,
    );

    var body = SafeArea(
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 32, bottom: 32, left: 15, right: 14),
          color: MyColor.blueBody,
          child: Container(
            decoration: BoxDecoration(
                color: MyColor.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 3,
                  )
                ],
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.all(20),
              children: <Widget>[
                Text(
                  'Survei dan Saran',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: MyColor.blackText1),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Survei dan saran Anda akan membantu kami dalam meningkatkan layanan untuk Anda',
                  style: TextStyle(color: MyColor.blackText1, fontSize: 12),
                ),
                SizedBox(
                  height: 24,
                ),
                Text(
                  'Seberapa mudah pinjaman di Tunaiku?',
                  style: TextStyle(
                      color: MyColor.blackText1, fontWeight: FontWeight.bold),
                ),
                Container(
                  margin: EdgeInsets.only(top: 12, bottom: 12),
                  child: Center(
                      child: RatingBar(
                    initialRating: 0,
                    itemPadding: EdgeInsets.only(left: 6, right: 6),
                    itemCount: _listStarRating.length,
                    minRating: 0,
                    itemSize: 40,
                    itemBuilder: (context, index) =>
                        Image.asset("assets/images/star_filled.png"),
                    onRatingUpdate: (double value) {
                      setState(() {
                        _selectedStarRating = _listStarRating[value] ?? '';
                      });
                    },
                  )),
                ),
                Center(
                    child: Text(
                  _selectedStarRating,
                  style: TextStyle(fontWeight: FontWeight.bold),
                )),
                Container(
                  height: 1,
                  width: double.infinity,
                  color: MyColor.grey,
                  margin: EdgeInsets.only(top: 16),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  'Bagaimana perasaan Anda saat meminjam di Tunaiku?',
                  style: TextStyle(
                      color: MyColor.blackText1, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  'Pilih salah satu saja',
                  style: TextStyle(color: MyColor.blackText2, fontSize: 12),
                ),
                Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Wrap(
                    children: _listEmotionRating
                        .map(
                          (it) => GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectedEmotionRating = it ?? '';
                              });
                            },
                            child: Container(
                              child: Text(
                                it,
                                style: TextStyle(
                                    color: _selectedEmotionRating == it
                                        ? MyColor.white
                                        : MyColor.blueBorder,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12),
                              ),
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: _selectedEmotionRating == it
                                      ? MyColor.blueBorder
                                      : MyColor.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(100.0)),
                                  border:
                                      Border.all(color: MyColor.blueBorder)),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ),
                Container(
                  height: 1,
                  width: double.infinity,
                  color: MyColor.grey,
                  margin: EdgeInsets.only(top: 16),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  'Tuliskan saran tambahan disini (tidak wajib)',
                  style: TextStyle(
                      color: MyColor.blackText1, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 16,
                ),
                TextFormField(
                  controller: _suggestion,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: MyColor.blueBorder)),
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                BlocBuilder<MyBloc, MyState>(
                                  builder: (BuildContext context, MyState state) => RaisedButton(
                    onPressed:
                        _selectedStarRating == '' || _selectedEmotionRating == ''
                            || state is SubmitOnProgress ? null
                            : () {
                                _onSubmitButtonPressed();
                              },
                    color: MyColor.green,
                    padding: EdgeInsets.all(12),
                    child: state is SubmitOnProgress ? SizedBox(height: 16, width: 16,child: CircularProgressIndicator()) : Text(
                      'KIRIM',
                      style: TextStyle(color: MyColor.white),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    return BlocListener<MyBloc, MyState>(
      listener: (BuildContext context, MyState state) {
        if (state is SubmitSuccess) {
          _showDialog();
        }
      },
      child: Scaffold(
        backgroundColor: MyColor.white,
        appBar: appBar,
        body: body,
      ),
    );
  }

  _showDialog() {
    showDialog(
        context: context,
        builder: (context) => Dialog(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Text(
                      'Survei dan Saran Anda sudah terkirim!',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: MyColor.blueButton),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Text(
                      'Terima kasih telah mengirimkan Survei dan Saran Anda.',
                      style: TextStyle(fontSize: 12, color: MyColor.blackText2),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 12, right: 12),
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        color: MyColor.blueButton,
                        padding: EdgeInsets.all(12),
                        child: Text(
                          'Saya Mengerti',
                          style: TextStyle(color: MyColor.white),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                      ),
                    ),
                  ],
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ));
  }
}
